" Vim syntax file
" Language: Fml
" Maintainer: Ryan Thompson <ryan.thompson@pherocity.com>
" Version:  0.0.1

if exists("b:current_syntax")
    finish
endif

if !exists("main_syntax")
    let main_syntax = 'fml'
endif

syn match fmlBegin  "^\s*\(&[^= ]\)\@!" nextgroup=fmlTag,fmlClassChar,fmlIdChar

syn keyword htmlArg contained textbox page pageheader combobox date 
syn cluster fmlComponent contains=fmlClassChar,fmlIdChar,fmlWrappedAttrs,fmlAttr,fmlInlineTagChar
syn match   fmlDocTypeKeyword "^\s*\(doctype\)\s\+" nextgroup=fmlDocType

syn keyword fmlTodo        TODO contained
syn keyword htmlTagName    contained script

syn match fmlTag           "\w\+[><]*"    contained contains=htmlTagName nextgroup=@fmlComponent
syn match fmlIdChar        "#{\@!"        contained nextgroup=fmlId
syn match fmlId            "\%(\w\|-\)\+" contained nextgroup=@fmlComponent
syn match fmlClassChar     "\."           contained nextgroup=fmlClass
syn match fmlClass         "\%(\w\|-\)\+" contained nextgroup=@fmlComponent
syn match fmlInlineTagChar "\s*:\s*"      contained nextgroup=fmlTag,fmlClassChar,fmlIdChar

syn region fmlWrappedAttrs matchgroup=fmlWrappedAttrsDelimiter start="\s*{\s*" skip="}\s*\""  end="\s*}\s*"  contained contains=fmlAttr 
syn region fmlWrappedAttrs matchgroup=fmlWrappedAttrsDelimiter start="\s*\[\s*" end="\s*\]\s*" contained contains=fmlAttr 
syn region fmlWrappedAttrs matchgroup=fmlWrappedAttrsDelimiter start="\s*(\s*"  end="\s*)\s*"  contained contains=fmlAttr 

syn match fmlAttr /\s*\%(\w\|-\)\+\s*=/me=e-1 contained contains=htmlArg nextgroup=fmlAttrAssignment
syn match fmlAttrAssignment "\s*=\s*" contained nextgroup=fmlWrappedAttrValue,fmlAttrString

syn region fmlWrappedAttrValue start="[^"']" end="\s\|$" contained contains=fmlAttrString nextgroup=fmlAttr,fmlInlineTagChar
syn region fmlWrappedAttrValue matchgroup=fmlWrappedAttrValueDelimiter start="{" end="}" contained contains=fmlAttrString nextgroup=fmlAttr,fmlInlineTagChar
syn region fmlWrappedAttrValue matchgroup=fmlWrappedAttrValueDelimiter start="\[" end="\]" contained contains=fmlAttrStringp nextgroup=fmlAttr,fmlInlineTagChar
syn region fmlWrappedAttrValue matchgroup=fmlWrappedAttrValueDelimiter start="(" end=")" contained contains=fmlAttrStringp nextgroup=fmlAttr,fmlInlineTagChar

syn region fmlAttrString start=+\s*"+ skip=+\%(\\\\\)*\\"+ end=+"\s*+ contained contains=fmlInterpolation,fmlInterpolationEscape nextgroup=fmlAttr,fmlInlineTagChar
syn region fmlAttrString start=+\s*'+ skip=+\%(\\\\\)*\\"+ end=+'\s*+ contained contains=fmlInterpolation,fmlInterpolationEscape nextgroup=fmlAttr,fmlInlineTagChar

syn region fmlInnerAttrString start=+\s*"+ skip=+\%(\\\\\)*\\"+ end=+"\s*+ contained contains=fmlInterpolation,fmlInterpolationEscape nextgroup=fmlAttr
syn region fmlInnerAttrString start=+\s*'+ skip=+\%(\\\\\)*\\"+ end=+'\s*+ contained contains=fmlInterpolation,fmlInterpolationEscape nextgroup=fmlAttr

syn match  fmlInterpolationEscape "\\\@<!\%(\\\\\)*\\\%(\\\ze#{\|#\ze{\)"

syn match fmlComment /^\(\s*\)[/].*\(\n\1\s.*\)*/ contains=fmlTodo
syn match fmlText    /^\(\s*\)[`|'].*\(\n\1\s.*\)*/ contains=fmlInterpolation

syn match fmlFilter /\s*\w\+:\s*/                            contained
syn match fmlHaml   /^\(\s*\)\<haml:\>.*\(\n\1\s.*\)*/       contains=@fmlHaml,fmlFilter

syn match fmlIEConditional "\%(^\s*/\)\@<=\[\s*if\>[^]]*]" contained containedin=fmlComment

hi def link fmlAttrString                       String
hi def link fmlBegin                            String
hi def link fmlClass                            Type
hi def link fmlAttr                             Type
hi def link fmlClassChar                        Type
hi def link fmlComment                          Comment
hi def link fmlDocType                          Identifier
hi def link fmlDocTypeKeyword                   Keyword
hi def link fmlFilter                           Keyword
hi def link fmlIEConditional                    SpecialComment
hi def link fmlId                               Identifier
hi def link fmlIdChar                           Identifier
hi def link fmlInnerAttrString                  String
hi def link fmlInterpolationDelimiter           Delimiter
hi def link fmlTag                              Reference
hi def link fmlText                             String
hi def link fmlTodo                             Todo
hi def link fmlWrappedAttrValueDelimiter        Delimiter
hi def link fmlWrappedAttrsDelimiter            Delimiter
hi def link fmlInlineTagChar                    Delimiter

hi link htmlTagName                             Reference
hi link htmlArg                                 Type

let b:current_syntax = "fml"
